<script async defer data-website-id="50683744-45c5-47ee-bd16-e3ce054f5939" src="https://umami.1samuel3.com/umami.js"></script>
# [Blog](https://blog.1samuel3.com)

**First of all**<br>
My code is shit, nothing I write works and you won't be able to read my spaghetti code. Have fun though!

---

### About
I chose to make a blog as a school project in 2020/2021. I wasn't able to finish it completely and it was like dead code laying somewhere until a few months later, when I decided to continue. There was just one problem: I wasn't able to read my shitcode and I thought it was garbage. I still liked the idea tho and thus comes version 2.0 as a rewrite with up- and downvotes, comments, image posts and much more! Stay tuned for further updates!

---

### Can I host it myself?
*aka. Code go yoink*<br>
Yes you can. You need to set up a seperate mysql Database tho.

---

### Latest changes (2021-05-12)
- Add FaQ Search
- Implement up- and downvote feature
- Update README.md

---

### Roadmap
- [ ] Implement commenting feature
- [ ] Add post detail page
- [ ] Add user detail page
- [ ] Add form to create posts
- [ ] Add posts dashboard to edit posts
- [ ] Add admin dashboard

### Contributors
- [sgart (admin@1samuel3.com)](https://git.1samuel3.com/sgart)
