<?php
class Database {

    // Set/Change login credentials in config/credentials.php

    private PDO $connection;

    // Connect to database when a new instance is created
    public function __construct() {
        require("../config/database.php");
        if (isset($host, $dbname, $username, $password)) {
            $this->connection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password) or die("Failed to connect to database: Missing credentials! <br><a href='mailto:admin@1samuel3.com'>Report to webmaster</a>");
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } else {
            die("Failed to connect to database: Missing credentials! <br><a href='../home/'>Back to homepage</a>");
        }
    }

    public function read(string $query, array $params = null, int $limit = null) {
        if (isset($limit)){
            $query = $query." LIMIT :limit";
        }
        $statement = $this->connection->prepare($query);
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (isset($limit)){
            $statement->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
        }
        if (isset($params)) {
            $statement->execute($params);
        } else {
            $statement->execute();
        }
        return $statement->fetchAll();
    }

    public function write(string $query, array $params = null) {
        $statement = $this->connection->prepare($query);
        if (isset($params)) {
            $statement->execute($params);
        } else {
            $statement->execute();
        }
    }
}