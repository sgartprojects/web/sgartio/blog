<?php
include("../classes/faqitem.php");
class FaQ {
    private database $database;

    public function __construct(Database $database = null)
    {
        if (!$database){
            include_once("../classes/database.php");
            $this->database = new database();
        }
        else
            $this->database = $database;
    }

    public function getAllItems():array
    {
        $rows = $this->database->read("SELECT * FROM faq");
        $items = array();
        foreach ($rows as $row){
            $name = $row["name"];
            $icon = $row["icon"];
            $title = $row["title"];
            $description = $row["description"];
            $searchTags = $row["search_tags"];
            $item = new FaQItem($name, $icon, $title, $description, $searchTags);
            array_push($items, $item);
        }
        return $items;
    }
}