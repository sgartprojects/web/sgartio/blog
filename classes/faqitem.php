<?php
class FaQItem {

    private string $name;
    private string $icon;
    private string $title;
    private string $description;
    private string $searchTags;

    public function __construct(string $name, string $icon, string $title, string $description, string $searchTags)
    {
        $this->name = $name;
        $this->icon = $icon;
        $this->title = $title;
        $this->description = $description;
        $this->searchTags = $searchTags;
    }

    public function getData():array
    {
        return array(
            "name" => $this->name,
            "icon" => $this->icon,
            "title" => $this->title,
            "description" => $this->description,
            "searchTags" => $this->searchTags
        );
    }
}