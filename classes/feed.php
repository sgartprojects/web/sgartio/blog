<?php
include_once("../classes/post.php");
class Feed{
    private Database $database;
    private array $filter;
    private string $term;

    public function __construct(Database $database = null)
    {
        if (!$database){
            include_once("../classes/database.php");
            $this->database = new Database();
        }
        else
            $this->database = $database;
    }

    public function getPublicFeed():array
    {
        $rows = $this->database->read("SELECT * FROM post WHERE archived = 0 ORDER BY post.created_at DESC");
        $posts = array();
        foreach ($rows as $row){
            $post = new Post($row["post_id"], $row["user_id"], date_create($row["created_at"]), date_create($row["last_changed"]), $row["archived"],
                $row["image_path"], $row["title"], $row["description"], $this->database);
            array_push($posts, $post);
        }
        return $posts;
    }

    public function showPosts(){
        $posts = $this->getPublicFeed();
        include_once("../helpers/timestamp.php");
        foreach ($posts as $post){
            $data = $post->getFeedData();
            include("../components/post.php");
        }
    }
}