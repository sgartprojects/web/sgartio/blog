<?php
require_once('../classes/session.php');
$sessionHandler = new Session();
$sessionHandler->openSession();
?>
<html lang="en">
<head>
    <!-- Title -->
    <title>Home - Blog</title>

    <!-- Icon -->
    <link rel="icon" type="img/png" href="../assets/logo.png"

    <!-- Meta -->
    <meta name="author" content="sgart">
    <meta name="description" content="Blog">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="../css/index.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- JS for theming -->
    <script src="../scripts/utils.js" type="text/javascript"></script>

    <!-- JS for sidenav -->
    <script src="../scripts/materialize.min.js" type="text/javascript"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            let sideNav = document.querySelectorAll('.sidenav');
            M.Sidenav.init(sideNav);
            let picturePopup = document.querySelectorAll('.materialboxed');
            M.Materialbox.init(picturePopup);
        });
    </script>
</head>
<body class="color--primary">
    <?php
    include("../components/navigation.php");
    ?>
    <main>
        <div class="row">
            <?php
            $amount = 24;
            include("../classes/feed.php");
            $feed = new Feed();
            $feed->showPosts();
            ?>
        </div>
    </main>
    <script src="../scripts/materialize.min.js"></script>
    <?php
    if (isset($_GET["toast"], $_GET["message"]) && $_GET["toast"]):
    ?>
    <script>
        M.toast({html: '<?= $_GET["message"] ?>', classes: 'aurora-green'});
    </script>
    <?php
    endif;
    ?>
    <?php
    if (isset($_GET["success"], $_GET["message"])):
        ?>
        <script>
            M.toast({html: '<?= $_GET["message"] ?>', classes: '<?= $_GET["success"] == '1' ? "aurora-green" : "aurora-red" ?>'});
        </script>
    <?php
    endif;
    ?>
</body>
</html>