// Login
function login(form){
    let valid = true;

    if (form.username.value === "") {
        document.getElementById('username').classList.add('invalid');
        valid = false;
    }
    else{
        document.getElementById('username').classList.remove('invalid');
    }
    if (form.password.value === ""){
        document.getElementById('password').classList.add('invalid');
        valid = false;
    }
    else{
        document.getElementById('password').classList.remove('invalid');
    }
    return valid;
}

// Registration

function checkFirstName(input){
    let valid = true;
    if (!input.value.match(/^[A-Za-z\- äëïöü]{0,50}$/)){
        valid = false;
        document.getElementById('firstNameMessage').setAttribute('data-error',
            'Firstname can only contain letters, spaces and minus and a maximum of 50 characters!');
        document.getElementById('firstName').classList.add('invalid');
    }
    else{
        document.getElementById('firstName').classList.remove('invalid');
    }
    return valid;
}

function checkLastName(input){
    let valid = true;
    if (!input.value.match(/^[A-Za-z\- äëïöü]{0,50}$/)){
        valid = false;
        document.getElementById('lastNameMessage').setAttribute('data-error',
            'Last name can only contain letters, spaces and minus and has to be between 2 and 50 characters long!');
        document.getElementById('lastName').classList.add('invalid');
    }
    else{
        document.getElementById('lastName').classList.remove('invalid');
    }
    return valid;
}

function checkUsername(input){
    let valid = true;
    if (!input.value.match(/^[A-z0-9_-]{3,50}$/)){
        valid = false;
        document.getElementById('usernameMessage').setAttribute('data-error',
            'Username can only contain letters, numbers, minus and underscores ' +
            'and has to be between 3 and 50 characters!');
        document.getElementById('username').classList.add('invalid');
    }
    else{
        document.getElementById('username').classList.remove('invalid');
    }
    return valid;
}

function checkEmail(input){
    let valid = true;
    if (input.value.length === 0){
        document.getElementById('email').classList.remove('invalid');
    }
    else if (input.value.length > 100){
        valid = false;
        document.getElementById('emailMessage').setAttribute('data-error',
            'Email can\'t exceed 100 caracters!');
        document.getElementById('email').classList.add('invalid');
    }
    else if (!input.value.match(/[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/)){
        valid = false;
        document.getElementById('emailMessage').setAttribute('data-error',
            'Please enter a valid email!');
        document.getElementById('email').classList.add('invalid');
    }
    else{
        document.getElementById('email').classList.remove('invalid');
    }
    return valid;
}

function checkPassword(input){
    let valid = true;
    if (!input.value.match(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{12,}$/)){
        valid = false;
        document.getElementById('passwordMessage').setAttribute('data-error',
            'This password has fewer than 12 characters or contains symbols which are not allowed!');
        document.getElementById('password').classList.add('invalid');
    }
    else{
        document.getElementById('password').classList.remove('invalid');
    }
    return valid;
}

function checkPasswordRedo(input, sourceInput = document.getElementById('password')){
    let valid = true;
    if (input.value.match(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{12,}$/)){
        valid = false;
        document.getElementById('passwordRedoMessage').setAttribute('data-error',
            'This password has fewer than 12 characters or contains symbols which are not allowed!');
        document.getElementById('passwordRedo').classList.add('invalid');
    }
    else{
        document.getElementById('passwordRedo').classList.remove('invalid');
    }

    if (input.value !== sourceInput.value){
        valid = false;
        document.getElementById('passwordRedoMessage').setAttribute('data-error',
            'Passwords don\'t match!');
        document.getElementById('passwordRedo').classList.add('invalid');
    }
    else if (input.value !== "") {
        document.getElementById('passwordRedo').classList.remove('invalid');
    }
    return valid;
}

function registration(form){
    return (edit(form) &&
        checkUsername(form.username) &&
        checkPassword(form.password) &&
        checkPasswordRedo(form.passwordRedo));
}

function edit(form){
    return (checkFirstName(form.firstName) &&
        checkLastName(form.lastName) &&
        checkEmail(form.email));
}

function userEdit(form){
    return (edit &&
        checkPassword(form.password) &&
        checkPasswordRedo(form.passwordRedo));
}

function changePassword(form){
    return checkPassword(form.oldPassword)
        && checkPassword(form.newPassword)
        && checkPasswordRedo(form.newPasswordRedo, form.newPassword)
}

// search

function checkSearch(form){
    return checkInput(form.term);
}

function checkInput(input){
    if (!!input.value.match(/^[A-zäëïöü ]*$/)){
        document.getElementById('term').classList.remove('invalid');
        return true;
    }
    else{
        document.getElementById('term').classList.add('invalid');
        return false;
    }
}

// post

function checkNewPost(form, type){
    if (type === "text"){
        return checkPostEdit(form);
    }
}

function checkPostEdit(form){
    return checkTitle(form.title) &&
        checkDescription(form.description);
}

function checkTitle(input){
    if (input.value.length === 0){
        return true;
    }
    else if (input.value.length >= 4 && input.value.length <= 20){
        document.getElementById('title').classList.remove('invalid');
        return true;
    }
    else{
        document.getElementById('titleMessage').setAttribute('data-error',
            'Title can\'t be longer than 20 characters long.');
        document.getElementById('title').classList.add('invalid');
        return false;
    }
}

function checkDescription(input) {
    if (input.value.length >= 4 && input.value.length <= 200){
        document.getElementById('description').classList.remove('invalid');
        return true;
    }
    else{
        document.getElementById('descriptionMessage').setAttribute('data-error',
            'Description can only be between 4 and 200 characters.');
        document.getElementById('description').classList.add('invalid');
        return false;
    }
}
